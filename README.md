<p align="center">
<a href="https://git.io/typing-svg"><img src="https://readme-typing-svg.demolab.com?font=Poppins&size=22&duration=3500&pause=800&center=true&random=false&width=435&lines=Hi+I+am+Bedirhan+Tong;Software+Developer;Currently+Learning+React.js" /></a>

🤓I’m currently learning **Web application developing with React.js and Flutter.**.👾Also working on **open source projects**.🤝I’m looking for help with **_my public repositories_** 💪

-📫 How to reach me **bedirhantongdev@gmail.com**

<div id="user-content-toc">
  <ul align="center">
    <summary><h2 style="display: inline-block">Technologies that I know👨🏻‍💻</h2></summary>
  </ul>
</div>

<p align="center">
  <a href="https://skillicons.dev">
    <img src="https://skillicons.dev/icons?i=javascript,ts,react,flutter,dotnet,git,firebase,ai,&perline=4" />
  </a>
</p>
<br>

<p align="center">
<table align="center">
<tr border="none">
<td width="50%" align="center">
  
  <img  align="center"  src="https://github-readme-stats.vercel.app/api?username=bedirhantong&theme=nightowl&show_icons=true&count_private=true" />
  <br></br>
  <img  title="🔥 Get streak stats for your profile at git.io/streak-stats" alt="Mark streak" src="https://github-readme-stats.vercel.app/api/pin/?username=bedirhantong&repo=threads_clone&theme=nightowl&hide_border=false" />

</td>

<td width="50%" align="center">

  <img  align="center"  src="https://github-readme-stats.anuraghazra1.vercel.app/api/top-langs/?username=bedirhantong&theme=nightowl&hide_border=false&no-bg=true&no-frame=true&langs_count=6"/>

  </td>
</tr>
</table>
